// Экзаменационное задание №1
// написать и протестировать функцию переворачивающую строку
#include <iostream>
#include <string>
using namespace std;
// функция переворачивает строку
// принимает указатель на символьный массив
// возвращает указатель на тот же массив
char *reverse(char *str) {
    // переменная длины строки
    int j = strlen(str);
    // переменная обмена
    char tmp;
    // половинный цикл перестановки
    for (int i = 0; i < j / 2; i++) {
        tmp = str[i];
        str[i] = str[j - i - 1];
        str[j - i - 1] = tmp;
    }
    return str;
}

int main() {
    // определяем символьный массив 50 максимальное число символов
    char str[50];
    cin >> str;
    cout << reverse(str) << endl;
    return 0;
}
