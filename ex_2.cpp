// Экзаменационное задание №2
// сформировать целочисленный массив из 15 элементов
// элементы массива ввести как случайные числа в диапозоне от -8 до 10
// Отсортировать массив по возростанию методом выбора
// Найти наибольшее значение разности между соседними элементами
#include <iostream>
#include <ctime>
using namespace std;
// вывод массива
void showArray(int *array, const int LEN) {
    for (int i = 0; i < LEN; i++) {
        cout << array[i] << ' ';
    }
    cout << endl;
}
// находит наибольшее значение разности между соседними элементами
int difference(int *array, const int LEN) {
    int difference = 0;
    for (int i = 0; i < LEN - 1; i++) {
        if (array[i] - array[i + 1] < difference) {
            difference = array[i] - array[i + 1];
        }
    }
    return difference;
}
// сортирует массив методом выбора
void selectSort(int *array, const int LEN) {
    int temp;
    for (int i = 0; i < LEN; i++) {
        temp = array[0]; // временная переменная для хранения перестановки
        for (int j = i + 1; j < LEN; j++) {
            if (array[i] > array[j]) {
                temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
        }
    }
}

int main() {
    const int LEN = 15;
    int array[LEN];
    // заполняет массив случайными числами
    srand(time(NULL));
    for (int i = 0; i < LEN; i ++) {
        array[i] = rand() % 19 - 8;
    }
    // выводи полученный массив
    cout << "Array:" << endl;
    showArray(array, LEN);
    // вывод разницы между элементами
    cout << "Max difference = " << difference(array, LEN) << endl;
    // вывод отсортированного массива
    cout << "Sort array:" << endl;
    selectSort(array, LEN);
    showArray(array, LEN);
    return 0;
}
